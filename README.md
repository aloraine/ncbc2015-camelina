# About #

This repository contains documents for an NC Biotechnology Center grant
requesting funds to find out if knocking out or over-expressing genes that affect seed size in Arabidopsis can make seeds larger in the model oilseed crop Camelina sativa.

It also contains reviews.

# Questions? #

* Contact Ann Loraine at aloraine@uncc.edu

# Copyright

All materials are copyright Ann Loraine.